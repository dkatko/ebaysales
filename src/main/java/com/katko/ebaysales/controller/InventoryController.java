package com.katko.ebaysales.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.katko.ebaysales.model.Inventory;
import com.katko.ebaysales.repository.InventoryRepository;

@RestController
public class InventoryController {

	@Autowired
	InventoryRepository inventoryRepository;

	@RequestMapping("/getall")
	public List<Inventory> getAllInventory() {
		System.out.println("Starting repository access");
		return inventoryRepository.findAllInventory();
	}

}
