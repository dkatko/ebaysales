package com.katko.ebaysales.controller;

import java.io.File;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExcelFileController {
	
	@RequestMapping("/getexcelfilename")
	File[] getFiles() {
		File dir = new File("D:\\Documents\\Ebay\\Books\\2015");
		File[] directoryListing = dir.listFiles();
		
		if (directoryListing != null) {
			for (File file : directoryListing) {
				System.out.println(file.getName());
			}
		}
		
		return directoryListing;
	}
}
