package com.katko.ebaysales.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
//@EnableAutoConfiguration
@EntityScan(basePackages = {"com.katko.ebaysales"})
@SpringBootApplication
public class EbaysalesApplication {

	public static void main(String[] args) {
		SpringApplication.run(EbaysalesApplication.class, args);
	}
}
