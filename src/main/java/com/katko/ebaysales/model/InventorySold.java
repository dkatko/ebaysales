package com.katko.ebaysales.model;

import javax.persistence.Entity;

@Entity
public class InventorySold extends BaseEntity{
	private double priceSold;
	private double shipping;
	private double ebayFees;
	private double shippingFees;
	private double paypalFees;
	private double netProfit;
	
	
	public double getPriceSold() {
		return priceSold;
	}
	public void setPriceSold(double priceSold) {
		this.priceSold = priceSold;
	}
	public double getShipping() {
		return shipping;
	}
	public void setShipping(double shipping) {
		this.shipping = shipping;
	}
	public double getEbayFees() {
		return ebayFees;
	}
	public void setEbayFees(double ebayFees) {
		this.ebayFees = ebayFees;
	}
	public double getShippingFees() {
		return shippingFees;
	}
	public void setShippingFees(double shippingFees) {
		this.shippingFees = shippingFees;
	}
	public double getPaypalFees() {
		return paypalFees;
	}
	public void setPaypalFees(double paypalFees) {
		this.paypalFees = paypalFees;
	}
	public double getNetProfit() {
		return netProfit;
	}
	public void setNetProfit(double netProfit) {
		this.netProfit = netProfit;
	}
	
}
