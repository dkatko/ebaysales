package com.katko.ebaysales.model;

import javax.persistence.Entity;

@Entity
public class InventorySource extends BaseEntity{
	
	private String name;
	private String adress;
	private double distance;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}

}
