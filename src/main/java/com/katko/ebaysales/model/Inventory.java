package com.katko.ebaysales.model;

import javax.persistence.Entity;

@Entity
public class Inventory extends BaseEntity{

	private String name;
	private double price;
	private double tax;
	private double totalPrice;
	private String size;
	private String type;
	
	
	
	public Inventory(String name, double price, double tax, double totalPrice, String size, String type, String uPC) {
		super();
		this.name = name;
		this.price = price;
		this.tax = tax;
		this.totalPrice = totalPrice;
		this.size = size;
		this.type = type;
		UPC = uPC;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUPC() {
		return UPC;
	}

	public void setUPC(String uPC) {
		UPC = uPC;
	}

	private String UPC;

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
