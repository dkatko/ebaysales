package com.katko.ebaysales.repository;

import org.springframework.data.repository.CrudRepository;

import com.katko.ebaysales.model.InventorySource;

public interface InventorySourceRepository extends CrudRepository<InventorySource, Long>{

}
