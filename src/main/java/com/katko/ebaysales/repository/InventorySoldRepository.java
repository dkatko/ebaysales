package com.katko.ebaysales.repository;

import org.springframework.data.repository.CrudRepository;

import com.katko.ebaysales.model.InventorySold;

public interface InventorySoldRepository extends CrudRepository<InventorySold, Long>{

}
