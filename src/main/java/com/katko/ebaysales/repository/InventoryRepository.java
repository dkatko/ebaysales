package com.katko.ebaysales.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.katko.ebaysales.model.Inventory;

public interface InventoryRepository extends CrudRepository<Inventory, Long>{
	List<Inventory> findAllInventory();
	List<Inventory> findAllByType();
	List<Inventory> findAllBySize();
}
